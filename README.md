# Build

```
podman build . -t quay.io/tbalsys/dovecot-ldap:2.3.13-r0
podman push quay.io/tbalsys/dovecot-ldap:2.3.13-r0
```

# Run

```
podman create --name mda \
  -e LDAP_SERVER_URI=ldap://localhost \
  -e LDAP_SEARCH_BASE=ou=users,dc=example,dc=com \
  -e LDAP_BIND_DN=cn=admin,dc=example,dc=com \
  -e LDAP_BIND_PW=admin \
  -e DOVECOT_USER_FILTER="(&(objectClass=PostfixBookMailAccount)(uid=%n))" \
  -e DOVECOT_PASS_FILTER="(&(objectClass=PostfixBookMailAccount)(uid=%n))" \
  -e DOVECOT_USER_ATTRS="homeDirectory=home,uidNumber=uid,gidNumber=gid" \
  -e DOVECOT_PASS_ATTRS="uid=user,userPassword=password" \
  -e DOVECOT_SSL_CERT="/certs/fullchain.cer" \
  -e DOVECOT_SSL_KEY="/certs/private.key" \
  -v ./mail:/var/mail \
  -v ./ssl:/certs:ro \
  quay.io/tbalsys/dovecot-ldap:2.3.13-r0
```
