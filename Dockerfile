FROM alpine:3.13

EXPOSE 993
EXPOSE 143
EXPOSE 995
EXPOSE 110
EXPOSE 24

RUN apk --no-cache add dovecot dovecot-ldap dovecot-lmtpd \
  && addgroup -g 1000 vmail \
  && adduser -u 1000 -G vmail -D -H -s /sbin/nologin vmail

COPY entrypoint.sh /entrypoint.sh

CMD ["/entrypoint.sh"]
