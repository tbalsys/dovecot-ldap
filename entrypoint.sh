#!/bin/sh

function setDovecotConf {
	KEY="$1"
	VALUE="$2"
	FILE="$3"
	echo "$FILE: $KEY=$VALUE"
	ESCAPED=$(printf '%s\n' "$VALUE" | sed -e 's/[\/&]/\\&/g')
	sed -i "1,/^#\?\s*$KEY\s*=.*\$/s/^#\?\s*$KEY\s*=.*\$/$KEY=$ESCAPED/" $FILE
}

sed -i /etc/dovecot/conf.d/10-auth.conf \
  -e 's/^!include auth-passwdfile/#!include auth-passwdfile/' \
  -e 's/^#!include auth-ldap/!include auth-ldap/'
# Outlook Express and Windows Mail works only with LOGIN mechanism, not the standard PLAIN
setDovecotConf "auth_mechanisms" "plain login" /etc/dovecot/conf.d/10-auth.conf

setDovecotConf "log_path" "/dev/stderr" /etc/dovecot/conf.d/10-logging.conf
setDovecotConf "auth_verbose" "yes" /etc/dovecot/conf.d/10-logging.conf
setDovecotConf "mail_location" "maildir:/var/mail/%u" /etc/dovecot/conf.d/10-mail.conf
chown vmail:vmail /var/mail
setDovecotConf "mail_uid" "1000" /etc/dovecot/conf.d/10-mail.conf
setDovecotConf "mail_gid" "1000" /etc/dovecot/conf.d/10-mail.conf

sed -i /etc/dovecot/conf.d/10-master.conf \
	-e "/service auth {/a \  inet_listener {\n    port = 12345\n  }"
sed -i /etc/dovecot/conf.d/10-master.conf \
	-e "/service lmtp {/a \  inet_listener {\n    port = 24\n  }"

if [ -n "$LDAP_SERVER_URI" ]; then
	setDovecotConf "uris" "$LDAP_SERVER_URI" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$LDAP_SEARCH_BASE" ]; then
	setDovecotConf "base" "$LDAP_SEARCH_BASE" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$LDAP_BIND_DN" ]; then
	setDovecotConf "dn" "$LDAP_BIND_DN" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$LDAP_BIND_PW" ]; then
	setDovecotConf "dnpass" "$LDAP_BIND_PW" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$DOVECOT_USER_FILTER" ]; then
	setDovecotConf "user_filter" "$DOVECOT_USER_FILTER" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$DOVECOT_PASS_FILTER" ]; then
	setDovecotConf "pass_filter" "$DOVECOT_PASS_FILTER" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$DOVECOT_USER_ATTRS" ]; then
	setDovecotConf "user_attrs" "$DOVECOT_USER_ATTRS" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$DOVECOT_PASS_ATTRS" ]; then
	setDovecotConf "pass_attrs" "$DOVECOT_PASS_ATTRS" /etc/dovecot/dovecot-ldap.conf.ext
fi

if [ -n "$DOVECOT_SSL_CERT" ]; then
  setDovecotConf "ssl_cert" "<$DOVECOT_SSL_CERT" /etc/dovecot/conf.d/10-ssl.conf
fi

if [ -n "$DOVECOT_SSL_KEY" ]; then
  setDovecotConf "ssl_key" "<$DOVECOT_SSL_KEY" /etc/dovecot/conf.d/10-ssl.conf
fi

exec dovecot -F
